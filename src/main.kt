fun calculaBonus(cargo: String, salario: Float): Float{
    var bonus: Float = salario
    if (cargo == "coordenador") {
        bonus = salario * 0.2f
    }
    else if(cargo == "Gerente Junior"){
        bonus = salario * 0.5f
    }
    else{
        bonus = salario * 2
    }
    return bonus
}

fun hello(nome: String): String{
    return "Olá, $nome"
}

// Função em uma única linha
fun hello2(nome: String): String = "Olá, $nome"
fun soma(a: Int, b: Int): Int = a + b

// if - else
fun maiorDeIdade(idade: Int) : Unit {
    if (idade >= 18)
    {
        println("Maior de idade!")
    }
    else
    {
        println("Menor de idade!")
    }
}
fun maiorDeIdade2(idade: Int): Boolean = idade >= 18

fun main() {
    val a = 10
    val b = 20
    val c = 30
    println("O bônus é ${calculaBonus(a, b, c)} ")
    println(hello("Anselmo"))

    // Operçações aritméticas
    var numero = 10
    println("numero + 1 = ${numero++}")
    println("numero - 1 = ${numero--}")

    /**
     OBS:
        - numero++ -> uso o número e incremento depois
        - ++numero -> incremento o número e uso depois
     */

    numero += 2
    println("numero + 2 = $numero")

    numero *= 2
    println("numero * 2 = $numero")

}